package de.larcado.sesam.model;

import android.support.annotation.NonNull;

public class Domain implements Comparable<Domain> {
    private String name;
    private String salt;
    private int profilID;
    private int usecount;

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getSalt() {
        return salt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProfilID() {
        return profilID;
    }

    public void setProfilID(int profilID) {
        this.profilID = profilID;
    }

    public int getUsecount() {
        return usecount;
    }

    public void setUsecount(int usecount) {
        this.usecount = usecount;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(@NonNull Domain domain) {
        return Integer.compare(domain.usecount, usecount);
    }
}
