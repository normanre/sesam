package de.larcado.sesam.model;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import de.larcado.sesam.Settings;

public class JsonStorage {
    private List<Domain> domains;
    private List<Profil> profile;

    private JsonStorage() {
    }

    public List<Domain> getDomains() {
        if (domains == null) {
            domains = new ArrayList<>();
        }
        return domains;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

    public List<Profil> getProfile() {
        if (profile == null) {
            profile = new ArrayList<>();
        }
        return profile;
    }

    public void setProfile(List<Profil> profile) {
        this.profile = profile;
    }

    public Profil getProfilById(int id) {
        for (Profil profil : profile) {
            if (profil.getId() == id) {
                return profil;
            }
        }
        return null;
    }

    public void save(Context context) {
        Gson gson = new Gson();
        String jsonInString = gson.toJson(this);
        try {
            File dir = new File(Environment.getExternalStorageDirectory() + "/SesamNorman/");
            dir.mkdir();
            String namFile = Environment.getExternalStorageDirectory() + "/SesamNorman/sesam.json";
            File file = new File(namFile);
            try (FileOutputStream fOut = new FileOutputStream(file)) {
                try (OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut)) {
                    myOutWriter.append(jsonInString);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Write failure: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public static JsonStorage load(Context context) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/SesamNorman/");
        dir.mkdir();
        File file = new File(Environment.getExternalStorageDirectory() + "/SesamNorman/sesam.json");
        try {
            Gson gson = new Gson();
            return gson.fromJson(new FileReader(file), JsonStorage.class);
        } catch (FileNotFoundException e) {
            Settings settings = new Settings(context);

            JsonStorage jsonStorage = new JsonStorage();
            jsonStorage.setDomains(new ArrayList<>());
            jsonStorage.setProfile(new ArrayList<>());
            Profil standard = new Profil();
            standard.setId(1);
            standard.setName("Default");
            standard.setLength(settings.getLengthOfPasswords());
            standard.setSonderzeichen(settings.getSpecialCharacters());
            jsonStorage.getProfile().add(standard);
            jsonStorage.save(context);

            return jsonStorage;
        }
    }
}
