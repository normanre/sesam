package de.larcado.sesam;

public interface StatusLogger {
    void onStatus(String status);
}
