package de.larcado.sesam.autofill;

import android.app.assist.AssistStructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.view.View.AUTOFILL_HINT_PASSWORD;

public class StructureParser {

    private final AssistStructure mStructure;
    private List<AssistStructure.ViewNode> mViewNodes;

    StructureParser(AssistStructure structure) {
        mStructure = structure;

    }

    public List<AssistStructure.ViewNode> parse() {
        mViewNodes = new ArrayList<>();
        int nodes = mStructure.getWindowNodeCount();

        for (int i = 0; i < nodes; i++) {
            AssistStructure.WindowNode windowNode = mStructure.getWindowNodeAt(i);
            AssistStructure.ViewNode viewNode = windowNode.getRootViewNode();
            traverseNode(viewNode);
        }
        return mViewNodes;
    }

    private void traverseNode(AssistStructure.ViewNode viewNode) {
        if (viewNode.getAutofillHints() != null && viewNode.getAutofillHints().length > 0) {
            // If the client app provides autofill hints, you can obtain them using:
            // viewNode.getAutofillHints();
            if (Arrays.asList(viewNode.getAutofillHints())
                    .contains(AUTOFILL_HINT_PASSWORD)) {
                mViewNodes.add(viewNode);
            }
        } else {
            // Or use your own heuristics to describe the contents of a view
            // using methods such as getText() or getHint().
            if ("password".equalsIgnoreCase(viewNode.getHint()) ||
                    "passwort".equalsIgnoreCase(viewNode.getHint())) {
                mViewNodes.add(viewNode);
            }
        }

        for (int i = 0; i < viewNode.getChildCount(); i++) {
            AssistStructure.ViewNode childNode = viewNode.getChildAt(i);
            traverseNode(childNode);
        }
    }
}
