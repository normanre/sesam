package de.larcado.sesam;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import de.larcado.sesam.model.Domain;
import de.larcado.sesam.model.Profil;

public class Sesam {

    @SuppressWarnings("serial")
    static public class CannotPerformOperationException extends Exception {
        public CannotPerformOperationException(String message, Throwable source) {
            super(message, source);
        }
    }

    public static String getPasswordSesam(final Activity activity, String master_passwd, Domain input_pwd, Profil profil, ProgressBar pb, TextView details) {
        setVisible(activity, pb, true);
        String password = sesam(activity, master_passwd, input_pwd, profil, status -> setDetails(activity, details, status));
        setVisible(activity, pb, false);
        return password;
    }

    public static String getPasswordSesam(Context context, String master_passwd, Domain input_pwd, Profil profil) {
        return sesam(context, master_passwd, input_pwd, profil, System.out::println);
    }

    private static String sesam(Context context, String master_passwd, Domain input_domain, Profil profil, StatusLogger logger) {
        String password = "";
        try {
            Settings settings = new Settings(context);
            logger.onStatus("importing settings...");
            String hash_string = input_domain.getName() + master_passwd;

            char[] passwordChars = hash_string.toCharArray();
            byte[] saltBytes = getSalt(input_domain);
            int PBKDF2_ITERATIONS = settings.getIterations();
            int HASH_BIT_SIZE = settings.getHashBitSize();

            logger.onStatus("domain + master password will be hashed...");

            byte[] hash = pbkdf2(passwordChars, saltBytes, PBKDF2_ITERATIONS, HASH_BIT_SIZE);

            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                logger.onStatus("to BigInteger...");
                outputStream.write(new byte[]{0});
                outputStream.write(hash);
                BigInteger x = new BigInteger(outputStream.toByteArray());

                logger.onStatus("put password together");
                password = convert_bytes_to_password(x, profil.getLength(), settings.getPasswordCharactersWithoutSpecial() + profil.getSonderzeichen());
                logger.onStatus("done");
            }
        } catch (IOException | CannotPerformOperationException e) {
            e.printStackTrace();
            logger.onStatus(e.getMessage());
        }
        return password;
    }

    private static byte[] pbkdf2(char[] password, byte[] salt, int iterations, int bits) throws CannotPerformOperationException {
        try {
            PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bits);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException ex) {
            throw new CannotPerformOperationException("Hash algorithm not supported.", ex);
        } catch (InvalidKeySpecException ex) {
            throw new CannotPerformOperationException("Invalid key spec.", ex);
        }
    }

    private static byte[] getSalt(Domain domain) {
        return Base64.getEncoder().encode(
                domain.getSalt().getBytes(StandardCharsets.UTF_8)
        );
    }

    private static String convert_bytes_to_password(BigInteger number, int length, String PASSWORD_CHARACTERS) {
        BigInteger anzahlPwdChars = BigInteger.valueOf(PASSWORD_CHARACTERS.length());
        String password = "";
        while (number.compareTo(BigInteger.ZERO) == 1 && password.length() < length) {
            password += PASSWORD_CHARACTERS.charAt(number.mod(anzahlPwdChars).intValue());
            number = number.divide(anzahlPwdChars);
        }
        return password;
    }

    private static void setDetails(Activity activity, TextView t, String s) {
        activity.runOnUiThread(() -> t.setText(s));
    }

    private static void setVisible(Activity activity, ProgressBar pb, boolean bool) {
        activity.runOnUiThread(() -> pb.setVisibility(bool ? View.VISIBLE : View.GONE));
    }
}
